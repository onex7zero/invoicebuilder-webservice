package tw.teddysoft.bdd.domain.searchCompanyNameOrVatId;

/**
 * Created by user on 2017/4/15.
 */
public class DefaultInputCompanyNameOrVatId implements InputCompanyNameOrVatId {
    private String vatId = "";
    private String companyName = "";

    public static DefaultInputCompanyNameOrVatId newInstance(){
        return new DefaultInputCompanyNameOrVatId();
    }

    @Override
    public DefaultInputCompanyNameOrVatId withVatId(String vatId) {
        if(vatId.matches("[0-9]{8}"))
            this.vatId = vatId;
        return this;
    }

    @Override
    public DefaultInputCompanyNameOrVatId withCompanyName(String companyName) {
        if(companyName.matches("[\\u4e00-\\u9fa5]+"))
            this.companyName = companyName;
        return this;
    }

    @Override
    public void search() {
        if( vatId.equals("") && companyName.equals("") ){
            companyName = "未輸入資料";
        }
        if ( (vatId.length()==8) && !(companyName.equals(""))){
            companyName = "";
        }
        if(companyName.equals("") && (vatId.length()==8)){
            companyName = SearchCompanyNameOrVatId.searchCompanyName(vatId);
        }
        else {
            vatId = SearchCompanyNameOrVatId.searchVatId(companyName);
        }
    }

    @Override
    public String getCompanyName(){
        return companyName;
    }

    @Override
    public String getVatId(){
        return vatId;
    }
}

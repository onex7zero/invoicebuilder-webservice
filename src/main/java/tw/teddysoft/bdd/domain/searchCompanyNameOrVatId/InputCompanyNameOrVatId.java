package tw.teddysoft.bdd.domain.searchCompanyNameOrVatId;

/**
 * Created by user on 2017/4/15.
 */
public interface InputCompanyNameOrVatId {

    public static InputCompanyNameOrVatId newInstance(){
        return new DefaultInputCompanyNameOrVatId();
    }

    public InputCompanyNameOrVatId withVatId(String vatId);

    public InputCompanyNameOrVatId withCompanyName(String companyName);

    public void search();

    public String getCompanyName();

    public String getVatId();
}

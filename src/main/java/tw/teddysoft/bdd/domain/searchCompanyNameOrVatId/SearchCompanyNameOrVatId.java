package tw.teddysoft.bdd.domain.searchCompanyNameOrVatId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by user on 2017/4/15.
 */
public class SearchCompanyNameOrVatId {

    public static String searchVatId(String companyName)  {
        StringBuffer url = new StringBuffer("http://company.g0v.ronny.tw/api/search?q=");
        url.append(companyName);
        JSONObject json = readJsonFromUrl(url.toString());
        if( (int)json.get("found") == 0 ){
            return "未輸入或資料庫無紀錄此公司";
        }
        else if((int)json.get("found") > 1 ){
            return "查詢結果超過1筆，需檢查公司名稱是否為全名";
        }
        else {
            return json.getJSONArray("data").getJSONObject(0).getString("統一編號");
        }
    }

    public static String searchCompanyName(String vatId){
        StringBuffer url = new StringBuffer("http://company.g0v.ronny.tw/api/show/");
        url.append(vatId);
        JSONObject json = readJsonFromUrl(url.toString());
        if(json.has("error")){
            return "此編號無登記公司行號";
        }
        else if (json.getJSONObject("data").has("分公司狀況")){
            return json.getJSONObject("data").getJSONObject("財政部").getString("營業人名稱");
        }
        else if(json.getJSONObject("data").has("商業名稱")){
            return json.getJSONObject("data").getString("商業名稱");
        }
        else if(json.getJSONObject("data").has("名稱")){
            return json.getJSONObject("data").getString("名稱");
        }
        else{
            Object company = json.getJSONObject("data").get("公司名稱");
            if(  company instanceof JSONArray){
                return json.getJSONObject("data").getJSONArray("公司名稱").getString(0);
            }
            else {
                return json.getJSONObject("data").getString("公司名稱");
            }
        }
    }

    private static String readData(Reader rd)  {
        StringBuilder sb = new StringBuilder();
        int cp;
        try {
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
        }
        catch(IOException ex){}
        return sb.toString();
    }
    private static JSONObject readJsonFromUrl(String url){
        InputStream is;
        JSONObject json;
        try {
            is = new URL(url).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readData(rd);
            json = new JSONObject(jsonText);
            is.close();
            return json;
        }
        catch(JSONException jsonex){}
        catch (IOException ioex){}
        return null;
    }
}

Feature: Entering VAT ID and the company name
  In order to avoid errors
  As a Teddysoft employee
  I want to make sure the VAT ID and company name are consistency

  Scenario Outline: Successful search company Name using VAT ID
    When  I enter the VAT ID <VAT_ID>
    Then  I should see the company name <Company_Name>

    Examples:
    # VAT = Value Added Tax
      | VAT_ID      | Company_Name                          | notes           |
      | "53909614"  | "泰迪軟體科技有限公司"               | 有限公司        |
      | "86654648"  | "博碩文化股份有限公司"               | 股份有限公司    |
      | "18369941"  | "春田商社"                            | 商業登記        |
      | "03318403"  | "建新百貨無限公司"                    | 無限公司        |
      | "05001871"  | "義大利商飛多無限公司"                | 外商公司        |
      | "51375304"  | "東南旅行社股份有限公司台中分公司"   | 分公司          |
      | "92021164"  | "國立臺北科技大學"                    | 學校            |
      | "99999999"  | "此編號無登記公司行號"                | 邊界條件        |
      | ""          | "未輸入資料"                           | 邊界條件        |



  Scenario Outline: Successful search VAT ID using company name
    When  I enter the company name <Company_Name>
    Then  I should see the VAT ID <VAT_ID>

    Examples:
    # VAT = Value Added Tax
      | VAT_ID    | Company_Name               | notes           |
      | "53909614"  | "泰迪軟體科技有限公司"  | 有限公司        |
      | "86654648"  | "博碩文化股份有限公司"  | 股份有限公司    |
      | "18369941"  | "春田商社"               | 商業登記        |
      | "03318403"  | "建新百貨無限公司"       | 無限公司        |
      | "05001871"  | "義大利商飛多無限公司"   | 外商公司        |
      | "92021164"  | "國立臺北科技大學"       | 學校            |
      | "未輸入或資料庫無紀錄此公司"| "吃到飽有限公司"| 邊界條件        |

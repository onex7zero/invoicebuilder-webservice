package step;

import cucumber.api.java8.En;
import tw.teddysoft.bdd.domain.invoice.support.DomainSwitcher;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
/**
 * Created by user on 2017/4/15.
 */
public class SearchCompanyNameOrVatIdStep implements En {
    DomainSwitcher switcher;

    public SearchCompanyNameOrVatIdStep(DomainSwitcher switcher)  {
        this.switcher = switcher;
        When("^I enter the VAT ID \"([^\"]*)\"$", (String vatId) -> {
            switcher.getInputCompanyNameOrVatId().withVatId(vatId);
            switcher.getInputCompanyNameOrVatId().search();
        });
        Then("^I should see the company name \"([^\"]*)\"$", (String companyName) -> {
            assertThat(switcher.getInputCompanyNameOrVatId().getCompanyName(), is (companyName));
        });
        When("^I enter the company name \"([^\"]*)\"$", (String companyName) -> {
            switcher.getInputCompanyNameOrVatId().withCompanyName(companyName);
            switcher.getInputCompanyNameOrVatId().search();
        });
        Then("^I should see the VAT ID \"([^\"]*)\"$", (String vatId) -> {
            assertThat(switcher.getInputCompanyNameOrVatId().getVatId(), is (vatId));
        });
    }
}

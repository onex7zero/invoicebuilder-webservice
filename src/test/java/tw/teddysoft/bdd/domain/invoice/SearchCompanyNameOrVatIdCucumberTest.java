package tw.teddysoft.bdd.domain.invoice;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by user on 2017/4/18.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features/entering_vatid_and_company_name.feature",
        glue = {"step"},
        format = {"json:target/InputCompanyNameOrVatId/cucumber.json", "html:target/site/cucumber-pretty"}
)
public class SearchCompanyNameOrVatIdCucumberTest {
}


package tw.teddysoft.bdd.domain.invoice.support;

import tw.teddysoft.bdd.domain.searchCompanyNameOrVatId.InputCompanyNameOrVatId;

/**
 * Created by user on 2017/4/16.
 */
public class DomainSwitcher {
    InputCompanyNameOrVatId inputCompanyNameOrVatId;

    public DomainSwitcher(InputCompanyNameOrVatId inputCompanyNameOrVatId){
        this.inputCompanyNameOrVatId = inputCompanyNameOrVatId;
    }

    public InputCompanyNameOrVatId getInputCompanyNameOrVatId() {
        return inputCompanyNameOrVatId;
    }
}

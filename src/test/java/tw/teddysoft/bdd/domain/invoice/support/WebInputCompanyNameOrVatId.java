package tw.teddysoft.bdd.domain.invoice.support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import tw.teddysoft.bdd.domain.searchCompanyNameOrVatId.InputCompanyNameOrVatId;
import tw.teddysoft.bdd.domain.searchCompanyNameOrVatId.SearchCompanyNameOrVatId;


/**
 * Created by user on 2017/4/16.
 */
public class WebInputCompanyNameOrVatId implements InputCompanyNameOrVatId {
    private WebDriver driver = null;
    private String vatId = "";
    private String companyName = "";

    private WebDriver getWebDriver() {
        if (null == driver) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            driver = new ChromeDriver();
            driver.get("http://localhost:4567/searchCompanyNameOrVatId");
        }
        return driver;
    }

    @Override
    public InputCompanyNameOrVatId withVatId(String vatId) {
        getWebDriver().findElement(By.name("vatId")).sendKeys(vatId);
        return this;
    }

    @Override
    public InputCompanyNameOrVatId withCompanyName(String companyName) {
        getWebDriver().findElement(By.name("companyName")).sendKeys(companyName);
        return this;
    }

    @Override
    public void search() {
        try {
            getWebDriver().findElement(By.name("search")).click();
            vatId = getWebDriver().findElement(By.name("vatId")).getAttribute("value");
            companyName = getWebDriver().findElement(By.name("companyName")).getAttribute("value");
            if (vatId.equals("") && companyName.equals("")) {
                companyName = "未輸入資料";
            }
            if ((vatId.length() == 8) && !(companyName.equals(""))) {
                companyName = "";
            }
            if (companyName.equals("") && (vatId.length() == 8)) {
                companyName = SearchCompanyNameOrVatId.searchCompanyName(vatId);
            } else {
                vatId = SearchCompanyNameOrVatId.searchVatId(companyName);
            }
        } finally {
            getWebDriver().quit();
        }
    }

    @Override
    public String getCompanyName() {
        return companyName;
    }

    @Override
    public String getVatId() {
        return vatId;
    }
}

package tw.teddysoft.bdd.domain.invoice.tw.teddysoft.bdd.cucumber;

import org.junit.Test;
import tw.teddysoft.bdd.domain.searchCompanyNameOrVatId.SearchCompanyNameOrVatId;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by user on 2017/4/16.
 */
public class SearchCompanyNameOrVatIdTest {
    @Test
    public void Use_Company_Name_search_vatId_when_Company_is_有限公司(){
        assertThat(SearchCompanyNameOrVatId.searchVatId("泰迪軟體科技有限公司")).isEqualTo( "53909614" );
    }
    @Test
    public void Use_vatId_search_Company_Name_when_Company_is_有限公司(){
        assertThat(SearchCompanyNameOrVatId.searchCompanyName("53909614")).isEqualTo( "泰迪軟體科技有限公司" );
    }
    @Test
    public void Use_Company_Name_search_vatId_when_Company_is_學校(){
        assertThat(SearchCompanyNameOrVatId.searchVatId("國立臺北科技大學")).isEqualTo( "92021164" );
    }
    @Test
    public void Use_vatId_search_Company_Name_when_Company_is_學校(){
        assertThat(SearchCompanyNameOrVatId.searchCompanyName("92021164")).isEqualTo( "國立臺北科技大學" );
    }
    @Test
    public void Use_Company_Name_search_vatId_when_Company_is_外商公司(){
        assertThat(SearchCompanyNameOrVatId.searchVatId("義大利商飛多無限公司")).isEqualTo( "05001871" );
    }
    @Test
    public void Use_vatId_search_Company_Name_when_Company_is_外商公司(){
        assertThat(SearchCompanyNameOrVatId.searchCompanyName("05001871")).isEqualTo( "義大利商飛多無限公司" );
    }
    @Test
    public void Use_vatId_search_Company_Name_when_Company_is_分公司(){
        assertThat(SearchCompanyNameOrVatId.searchCompanyName("51375304")).isEqualTo( "東南旅行社股份有限公司台中分公司" );
    }
    @Test
    public void Use_Company_Name_search_vatId_when_result_is_多筆資料(){
        assertThat(SearchCompanyNameOrVatId.searchVatId("統一")).isEqualTo( "查詢結果超過1筆，需檢查公司名稱是否為全名" );
    }
}


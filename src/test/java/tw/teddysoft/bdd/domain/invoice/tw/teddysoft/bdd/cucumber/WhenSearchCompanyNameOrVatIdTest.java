package tw.teddysoft.bdd.domain.invoice.tw.teddysoft.bdd.cucumber;

import org.junit.Test;
import tw.teddysoft.bdd.domain.searchCompanyNameOrVatId.DefaultInputCompanyNameOrVatId;
import tw.teddysoft.bdd.domain.searchCompanyNameOrVatId.InputCompanyNameOrVatId;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

/**
 * Created by user on 2017/4/16.
 */
public class WhenSearchCompanyNameOrVatIdTest {
    @Test
    public void should_be_a_regular_DefaultInputCompanyNameOrVatId_when_given_normal_vatId(){
        InputCompanyNameOrVatId inputCompanyNameOrVatId = DefaultInputCompanyNameOrVatId.newInstance().
                withVatId("14070857");
        inputCompanyNameOrVatId.search();
        assertNotNull(inputCompanyNameOrVatId);
        assertThat(inputCompanyNameOrVatId.getVatId()).isEqualTo("14070857");
        assertThat(inputCompanyNameOrVatId.getCompanyName()).isEqualTo("蒂森電梯股份有限公司");
    }
    @Test
    public void should_be_a_regular_DefaultInputCompanyNameOrVatId_when_given_normal_companyName(){
        InputCompanyNameOrVatId inputCompanyNameOrVatId = DefaultInputCompanyNameOrVatId.newInstance().
                withCompanyName("蒂森電梯股份有限公司");
        inputCompanyNameOrVatId.search();
        assertNotNull(inputCompanyNameOrVatId);
        assertThat(inputCompanyNameOrVatId.getVatId()).isEqualTo("14070857");
        assertThat(inputCompanyNameOrVatId.getCompanyName()).isEqualTo("蒂森電梯股份有限公司");
    }
    @Test
    public void test_that_an_DefaultInputCompanyNameOrVatId_is_empty_all(){
        InputCompanyNameOrVatId inputCompanyNameOrVatId = DefaultInputCompanyNameOrVatId.newInstance()
                .withCompanyName("").withVatId("");
        inputCompanyNameOrVatId.search();
        assertNotNull(inputCompanyNameOrVatId);
        assertThat(inputCompanyNameOrVatId.getCompanyName()).isEqualTo("未輸入資料");
        assertThat(inputCompanyNameOrVatId.getVatId()).isEqualTo("未輸入或資料庫無紀錄此公司");
    }
    @Test
    public void test_that_an_DefaultInputCompanyNameOrVatId_has_different_input(){
        InputCompanyNameOrVatId inputCompanyNameOrVatId = DefaultInputCompanyNameOrVatId.newInstance()
                .withCompanyName("博碩文化股份有限公司").withVatId("53909614");
        inputCompanyNameOrVatId.search();
        assertNotNull(inputCompanyNameOrVatId);
        assertThat(inputCompanyNameOrVatId.getCompanyName()).isEqualTo("泰迪軟體科技有限公司");
        assertThat(inputCompanyNameOrVatId.getVatId()).isEqualTo("53909614");
    }
}
